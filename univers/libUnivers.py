'''
Ce module propose les trois univers pour la génération de nom. On peut récupérer tous les 
univers en important directement le tableau tabUnivers défini après les dictionnaires.
'''

#Dictionnaire de correspondances nom et prénom pour l'univers de Borderlands
Borderlands = {
    id : 0,
    "universName" : "Borderlands",
    "firstname" : {"position" : 1,"A" : "Mad","B" : "Dr Zed","C" : "Lilith","D" : "Roland","E" : "Brick","F" : "Mordecaï","G" : "Krieg","H" : "Gaige","I" : "Salvador","J" : "Axton","K" : "Maya","L" : "Zer0","M" : "Amara","N" : "FL4K","O" : "CL4P-TP","P" : "Moze","Q" : "Tiny","R" : "Patricia","S" : "Marcus","T" : "Sir Alistair","U" : "Handsome","V" : "Zane","W" : "Butt","X" : "Athena","Y" : "Wihelm","Z" : "Nisha"},
    "name" : {"position" : 1,"A" : "Moxxi","B" : "Blanco","C" : "the Siren","D" : "the Soldier","E" : "the Berserker","F" : "the Hunter","G" : "the Psycho","H" : "the Mechromancer","I" : "the Gunzerker","J" : "the Commando","K" : "the Siren","L" : "the Assassin","M" : "the Siren","N" : "the Beast Master","O" : "the FragTrap","P" : "the Gunner","Q" : "Tina","R" : "Tannis","S" : "Kincaid","T" : "Hammerlock","U" : "Jack","V" : "the Operative","W" : "Stalion","X" : "the Gladiator","Y" : "the Enforcer","Z" : "the Lawbringer"}
}
#Dictionnaire de correspondances nom et prénom pour l'univers de The Witcher
TheWitcher = {
    id : 1,
    "universName" : "The Witcher",
    "firstname" : {"position" : 3,"A" : "Cirilla","B" : "Crach","C" : "Olgierd","D" : "Philippa","E" : "Yennefer","F" : "Vernon","G" : "Keira","H" : "Geralt","I" : "Triss","J" : "Emhyr","K" : "Eredin","L" : "Carantir","M" : "Sigismund","N" : "Gaunter","O" : "Dettlaff","P" : "Morvran","Q" : "Sylvia","R" : "Julian","S" : "Zoltan","T" : "Radovid V","U" : "Lady","V" : "King","W" : "Regis","X" : "Cirilla","Y" : "Baron","Z" : "Cerys"},
    "name" : {"position" : 2,"A" : "Riannon","B" : "an Craite","C" : "von Everec","D" : "Eilhart","E" : "von Vengerberg","F" : "Roche","G" : "Metz","H" : "of Rivia","I" : "Merigold","J" : "var Emreis","K" : "Bréac Glas","L" : "Ar-Feiniel","M" : "Dijkstra","N" : "O'Dimm","O" : "van der Eretein","P" : "Voorhis","Q" : "Anna","R" : "Pankratz","S" : "Chivay","T" : "the Stern","U" : "of the Lake","V" : "Foltest","W" : "Terzieff-Godefroy","X" : "Fiona","Y" : "of Casadei","Z" : "an Craite"}
}
#Dictionnaire de correspondances nom et prénom pour l'univers de Tolkien
Tolkien = {
    id : 2,
    "universName" : "Tolkien",
    "firstname" : {"position" : 1,"A" : "Bilbo","B" : "Lord","C" : "Ancalagon","D" : "Gandalf","E" : "Gollum","F" : "Samwise","G" : "Boromir","H" : "Celebrimbor","I" : "Meriadoc","J" : "Peregrin","K" : "Legolas","L" : "Aragorn II","M" : "Saruman","N" : "Theodon","O" : "Treebeard","P" : "Melkov","Q" : "Sauron","R" : "Arwen","S" : "Azog","T" : "Lady","U" : "Gimli","V" : "Witch-King","W" : "Smaug","X" : "Gothmog","Y" : "Khamûl","Z" : "Grima"},
    "name" : {"position" : 4,"A" : "Baggins","B" : "Elron","C" : "the Black","D" : "the Grey","E" : "Smeagol","F" : "Gamgee","G" : "Captain of the White Tower","H" : "Lord of Eregion","I" : "Brandybuck","J" : "Took","K" : "Sindarin Elf of the Woodland Realm","L" : "Elessar Telcondar","M" : "the White","N" : "King of Rohan","O" : "the Ent","P" : "the First Dark Lord","Q" : "the Dark","R" : "Undómiel","S" : "the Defiler","T" : "Galadriel","U" : "Lockbearer","V" : "of Angmar","W" : "the Stupendus","X" : "Lord of Balrogs","Y" : "the Easterling","Z" : "Wormtongue"}
}
#Le champ "position" de "firstname" et "name" indique la position de la lettre à choisir dans le nom ou prenom de l'utilisateur

#Tableau répertoriant les dictionnaires des différents univers
tabUnivers = [Borderlands, TheWitcher, Tolkien]