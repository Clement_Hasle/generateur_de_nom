'''
Ce module propose deux fonctions de verification du nom et prénom permettant de 
s'assurer que la génération d'un nouveau nom peut être effectuée.
'''

from univers.libUnivers import tabUnivers

"""
VerificationLength : vérifie que le nom et prénom de l'utilisateur sont assez long pour générer un nom lié à un certain univers.
IN : 
    firstname  :  chaine de caractère correspondant au prénom de l'utilisateur.
    name       :  chaine de caractère correspondant au nom de l'utilisateur.
    numUnivers :  id de l'univers pour lequel on vérifie les longueur.
OUT :
    longEnough : booléen égale à vrai si les nom et prénom sont assez long sinon égale à faux.
"""
def VerificationLength(firstname, name, numUnivers):
    longEnough = True
    for univers in tabUnivers:
        if univers[id] == numUnivers:
            if (len(firstname) < univers["firstname"]["position"]):
                longEnough = False
            if (len(name) < univers["name"]["position"]):
                longEnough = False
    return longEnough

"""
VerificationChar : vérifie qu'un caractère est une lettre de l'alphabet.
IN : 
    char     :  caractère à tester.
OUT :
    isLetter : booléen égale à vrai si char est une lettre de l'alphabet sinon faux.
"""
def VerificationChar(char):
    isLetter = True
    if ((char < 'A' or char > 'Z') and (char < 'a' or char > 'z')):
        isLetter = False
    return isLetter
