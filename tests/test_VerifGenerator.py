import unittest
from generator import NameGenerator
from verification.verif import VerificationLength, VerificationChar
from univers.libUnivers import tabUnivers

'''
Ce jeu de tests permet de tester, à la fois, la fonction de génération du programme
principal et les fonctions de vérification du nom et prénom.
'''


class TestGenerateurFunctions(unittest.TestCase):

    def testNameGenerator(self):
        self.assertEqual(NameGenerator("Clement", "HASLE", 0), "Lilith the Mechromancer")
        self.assertEqual(NameGenerator("Clement", "HASLE", 1), "Yennefer Riannon")
        self.assertEqual(NameGenerator("Clement", "HASLE", 2), "Ancalagon Elessar Telcondar")


    def testVerificationLength(self):
        self.assertEqual(VerificationLength("Clément", "HASLE", 0), True)
        self.assertEqual(VerificationLength("", "",  0), False)
        self.assertEqual(VerificationLength("Clément","HASLE", 1), True)
        self.assertEqual(VerificationLength("OK", "rgqrgsrdfg", 1), False)
        self.assertEqual(VerificationLength("", "",  1), False)
        self.assertEqual(VerificationLength("Clément", "HASLE", 2), True)
        self.assertEqual(VerificationLength("Mattéo", "ok", 2), False)
        self.assertEqual(VerificationLength("", "",  2), False)
        
    
    def testVerificationChar(self):
        self.assertEqual(VerificationChar("2"), False)
        self.assertEqual(VerificationChar("é"), False)
        self.assertEqual(VerificationChar("!"), False)
        self.assertEqual(VerificationChar(" "), False)
        self.assertEqual(VerificationChar("°"), False)
        
        self.assertEqual(VerificationChar("a"), True)
        self.assertEqual(VerificationChar("A"), True)
        self.assertEqual(VerificationChar("d"), True)
        self.assertEqual(VerificationChar("P"), True)