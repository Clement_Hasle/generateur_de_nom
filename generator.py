from tkinter.messagebox import *
from tkinter import * 
from univers.libUnivers import tabUnivers
from verification.verif import VerificationLength, VerificationChar

"""
NameGenerator : permet de renvoyer un nom lié à un univers grâce au nom et prénom de l'utilisateur.
IN : 
    firstname       : chaine de caractère correspondant au prénom de l'utilisateur.
    name            : chaine de caractère correspondant au nom de l'utilisateur.
    numUnivers      : id de l'univers pour lequel on génère le nom.
OUT :
    universFullname : chaine de caractère correspondant au nom généré que l'on retourne.
"""
def NameGenerator(firstname, name, numUnivers):
    for univers in tabUnivers:
        if univers[id] == numUnivers:
            universFirstname = univers["firstname"][firstname[univers["firstname"]["position"] - 1].upper()]
            universName = univers["name"][name[univers["name"]["position"] - 1].upper()] 
            universFullname = universFirstname + " " + universName
    return universFullname

"""
StartGeneration : lance la génération du nom en fonction du prénom et du nom entrés par l'utilisateur.
Appelle donc les fonctions de vérification et la fonction de génération de nom si les vérification sont OK.
Cette fonction est appelée par le bouton de validation.
OUT :
    Affiche un message d'erreur si le prénom ou le nom n'est pas correct sinon affiche le nom généré dans 
    le label prévu à cet effet.
"""
def StartGeneration():
    selectedUnivers = dict
    for univers in tabUnivers:
        if univers["universName"] == universSelected.get():
            selectedUnivers = univers
    if(VerificationLength(firstname.get(), name.get(), selectedUnivers[id])):
        if (VerificationChar(firstname.get()[univers["firstname"]["position"]-1]) and VerificationChar(name.get()[univers["name"]["position"]-1])):
            outIndicationLabel.config(text = "Voici votre nom dans l'univers de %s :" % selectedUnivers["universName"])
            outLabel.config(text = NameGenerator(firstname.get(), name.get(), selectedUnivers[id]))
        else:
            showwarning("Erreur entrée", "Le nom ou le prénom contient un caractère spécial ne pouvant être associé à un champ du dictionnaire de correspondance.")
    else:
        showwarning("Nom ou Prénom trop court", 'Nom ou Prénom trop court.\nGénération impossible pour l\'univers de %s.' % selectedUnivers["universName"])       

#Pour effacer le message des Entry name et firstname au clic
def onClickFirstnameEntry(event):
    firstnameEntry.delete(0, len(firstname.get()))
def onClickNameEntry(event):
    nameEntry.delete(0, len(name.get()))

if __name__=="__main__":
    frame = Tk()
    frame.title("Générateur de nom")
    frame.columnconfigure(0, weight=1)
    frame.columnconfigure(1, weight=1)
    frame.rowconfigure(0, weight=1)
    frame.rowconfigure(1, weight=1)
    frame.rowconfigure(2, weight=1)
    frame.rowconfigure(3, weight=1)
    frame.rowconfigure(4, weight=1)
    frame.rowconfigure(5, weight=1)

    #Zone d'indication du prénom
    firstnameLabel = Label(frame, text = "Prénom :")
    firstnameLabel.grid(row = 0, column = 0, sticky="ew")

    #Zone de saisie du prénom 
    firstname = StringVar()
    firstname.set("Entrez votre prénom")
    firstnameEntry = Entry(frame, textvariable = firstname)
    firstnameEntry.grid(row = 0, column = 1, sticky='ew')
    firstnameEntry.bind('<Button-1>', onClickFirstnameEntry)

    #Zone d'indication du nom
    nameLabel = Label(frame, text = "Nom :")
    nameLabel.grid(row = 1, column = 0, sticky="ew")

    #Zone de saisie du nom 
    name = StringVar()
    name.set("Entrez votre nom")
    nameEntry = Entry(frame, textvariable = name)
    nameEntry.grid(row = 1, column = 1, sticky='ew')
    nameEntry.bind('<Button-1>', onClickNameEntry)

    #Zone d'indication de l'univers
    nameLabel = Label(frame, text = "Univers :")
    nameLabel.grid(row = 2, column = 0, sticky="ew")

    #Liste déroulante des univers
    tabNameUnivers = ['']*len(tabUnivers)
    for univers in tabUnivers:
        tabNameUnivers[univers[id]] = univers["universName"] #Création d'un tableau regroupant les noms des univers pour la liste déroulante
    universSelected = StringVar(frame)
    universSelected.set(tabNameUnivers[0])
    listUnivers = OptionMenu(frame, universSelected, *tabNameUnivers)
    listUnivers.grid(row = 2, column = 1, sticky='ew')

    #Zone d'indication pour l'affichage du résultat
    outIndicationLabel = Label(frame)
    outIndicationLabel.grid(row = 4, column=0, columnspan = 2)

    #Zone d'affichage du résultat
    outLabel = Label(frame)
    outLabel.grid(row = 5, column=0, columnspan = 2)
    
    #Bouton de validation
    button_validation = Button(frame, text="Valider", command = StartGeneration)
    button_validation.grid(row = 3, column = 0, columnspan = 2)
    
    frame.mainloop()